======================================= BÀI 1 ========================================

const rawCategories = [
  {
     id: 1,
     label: 'Foods',
     parentId: null,
     level: 0,
  },
  {
     id: 2,
     label: 'Fruits',
     parentId: 1,
     level: 1,
  },
  {
     id: 3,
     label: 'Banana',
     parentId: 2,
     level: 2,
  },
  {
     id: 4,
     label: 'Apple',
     parentId: 2,
     level: 2,
  },
  {
     id: 5,
     label: 'Strawberry',
     parentId: 2,
     level: 2,
  },
  {
     id: 6,
     label: 'Vegetables',
     parentId: 1,
     level: 1
  },
  {
     id: 7,
     label: 'Carrot',
     parentId: 6,
     level: 2,
  },
  {
     id: 8,
     label: 'Drinks',
     parentId: null,
     level: 0,
  },
]


// results
[
  {
    id: 1,
    label: 'Foods',
    childrens: [
      {
        id: 2,
        label: 'Fruits',
        children: [
          {
            id: 3,
            label: 'Banana'
          },
          {
            id: 4,
            label: 'Apple'
          },
          {
            id: 5,
            label: 'Strawberry'
          }
        ]
      },
      {
        id: 6,
        label: 'Vegetables',
        children: [
          {
            id: 7,
            label: 'Carrot'
          },
        ]
      }
    ]
  },
  {
    id: 8,
    label: 'Drinks',
    childrens: []
  }
]

// function
const convertedCategoriesByLevel = (rawCategories) => {
  const convertedCategories = [];

  // doing something
  
  return convertedCategories;

}
// viết 1 hàm nhận raw trả về kết quả cho converted 

======================================= BÀI 2 ========================================
Cho object1 = {} và object2 = {}. Viết hàm kiểm tra 2 object có bằng nhau ko?


======================================= BÀI 3 ========================================
Cho mảng gồm các số. Tính trung bình cộng của các số lẻ trong mảng
input: numbers = [ 12, 84, 1, 3, 83, 90, 23, 44, 5, 7]
output: oddNumbers = [1, 3, 83, 23, 5, 7] => averageOfNumbers = 20.333
