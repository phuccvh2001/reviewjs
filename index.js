const rawCategories = [
  {
    id: 1,
    label: "Foods",
    parentId: null,
    level: 0,
  },
  {
    id: 2,
    label: "Fruits",
    parentId: 1,
    level: 1,
  },
  {
    id: 3,
    label: "Banana",
    parentId: 2,
    level: 2,
  },
  {
    id: 4,
    label: "Apple",
    parentId: 2,
    level: 2,
  },
  {
    id: 5,
    label: "Strawberry",
    parentId: 2,
    level: 2,
  },
  {
    id: 6,
    label: "Vegetables",
    parentId: 1,
    level: 1,
  },
  {
    id: 7,
    label: "Carrot",
    parentId: 6,
    level: 2,
  },
  {
    id: 8,
    label: "Drinks",
    parentId: null,
    level: 0,
  },
];

const arrRes = [
  {
    id: 1,
    label: "Foods",
    childrens: [
      {
        id: 2,
        label: "Fruits",
        children: [
          {
            id: 3,
            label: "Banana",
          },
          {
            id: 4,
            label: "Apple",
          },
          {
            id: 5,
            label: "Strawberry",
          },
        ],
      },
      {
        id: 6,
        label: "Vegetables",
        children: [
          {
            id: 7,
            label: "Carrot",
          },
        ],
      },
    ],
  },
  {
    id: 8,
    label: "Drinks",
    childrens: [],
  },
];

// Bước 1 duyệt qua phần tử trong mảng
// bước 2 thêm thuộc tính obj vào cho level 0 và level 1
// bước 3 0 là cha của thằng 1 || 1 là cha của thằng 2
// Bước 4 thêm những thằng có parenId = 1 vào mảng chillDren của thằng parentId === null && label = 'Food'
// bước tiếp theo duyệt những thằng có parentID === 2 push vào mảng children của thằng cha

const addChildrenForArr = (rawCategories) => {
  if (!Array.isArray(rawCategories) || rawCategories.length === 0) return;
  let newArr = rawCategories.map((item) => {
    if (item.level === 0 || item.level === 1) {
      return { ...item, children: [] };
    } else {
      return item;
    }
  });

  //   ? FILTER
  const filterParentIDSame1 = newArr.filter((item) => item.parentId === 1);
  const filterParenIDSame2 = newArr.filter((item) => item.parentId === 2);
  const filterParenIDSame6 = newArr.filter((item) => item.parentId === 6);

  //   ? FIND

  const findLabelFood = newArr.find((item) => item.label === "Foods");
  findLabelFood.children.push(...filterParentIDSame1);

  findLabelFood.children.map((item) => {
    if (item.label === "Fruits") {
      item.children.push(...filterParenIDSame2);
    } else {
      item.children.push(...filterParenIDSame6);
    }
  });

  //   const findLabelFruits = findLabelFood.children.find(
  //     (item) => item.label === "Fruits"
  //   );
  //   findLabelFruits.children.push(...filterParenIDSame2);
  //   console.log("findLabelFruits", findLabelFruits);

  //   newArr.map((item, i) => {
  //     if (item.label === "Foods") {
  //       item.children.push(...filterParentIDSame1);
  //     }
  //   });

  newArr = newArr.filter((item) => item.level === 0);
  newArr.forEach((item) => {
    delete item.parentId;
    delete item.level;
    item.children.forEach((fruits) => {
      delete fruits.parentId;
      delete fruits.level;
      fruits.children.forEach((childFruits) => {
        delete childFruits.parentId;
        delete childFruits.level;
      });
    });
  });

  console.log(newArr);

  return newArr;
};
addChildrenForArr(rawCategories);

// ----------------------------> bai` 2 <-----------------------------------

const obj1 = {
  name: "phuc",
  age: 12,
};
const obj2 = {
  name: "toan",
};

const checkLengthOfObj = (obj1, obj2) => {
  if (obj1 === obj2) console.log(true);
  const objOneLength = Object.keys(obj1).length;
  const objTowLength = Object.keys(obj2).length;

  console.log("objOneLength", objOneLength);
  console.log("objTowLength", objTowLength);
  objOneLength === objTowLength ? console.log(true) : console.log(false);
};

checkLengthOfObj(obj1, obj2);

// ---------------------------------> bài 2  <--------------------------------

const numberList = [12, 84, 1, 3, 83, 90, 23, 44, 5, 7];

const calcAverage = (numberList) => {
  let countLength = 0;
  const calcSum = numberList.reduce((sum, currentValue) => {
    if (currentValue % 2 !== 0) {
      countLength++;
      return (sum += currentValue);
    }
    return sum;
  }, 0);
  console.log(calcSum);
  console.log(countLength);
  return console.log(Number((calcSum / countLength).toFixed(3)));
};

calcAverage(numberList);
